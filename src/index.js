require('regenerator-runtime/runtime')
const THREE = require('three')
const DAT = require('dat.gui')
// const gui = new DAT.GUI()
const OrbitControls = require('../node_modules/three/examples/jsm/controls/OrbitControls').OrbitControls
const RefractionMaterial = require('./refraction-material').default
const loadTexture = require("./util/texture-loader").default

let vp = {
    width: window.innerWidth,
    height: window.innerHeight,
    dpr: Math.min(devicePixelRatio, 2 || 1)
};
let geometry = new THREE.BoxGeometry(1, 1, 1);
var material = new THREE.MeshBasicMaterial({ color: 'black' });
var cube = new THREE.Mesh(geometry, material);

let camera = new THREE.PerspectiveCamera(
    50,
    vp.width / vp.height,
    0.1,
    1000
);
camera.position.z = 5
renderer = new THREE.WebGLRenderer({ antialias: true })
renderer.setSize(vp.width, vp.height);
renderer.setPixelRatio(vp.dpr);
renderer.autoClear = false;
renderer.setSize(window.innerWidth, window.innerHeight);
renderer.autoClear = false;
document.body.appendChild(renderer.domElement);
var controls = new OrbitControls(camera, renderer.domElement);

let scene = new THREE.Scene()
scene.background = new THREE.Color('white')

window.addEventListener('resize', onWindowResize, false);

///////////////////start shading//////////////////////////////////////////

//setup

let orthoCamera = new THREE.OrthographicCamera(
    vp.width / -2,
    vp.width / 2,
    vp.height / 2,
    vp.height / -2,
    1,
    1000
);

orthoCamera.layers.set(1);
orthoCamera.position.z = 5
let envFbo = new THREE.WebGLRenderTarget(
    vp.width * vp.dpr,
    vp.height * vp.dpr
);
// texture
let quad = loadTexture("textures3.jpg").then((t) => {
    quad = new THREE.Mesh(
        new THREE.PlaneBufferGeometry(),
        new THREE.MeshBasicMaterial({ 
            map: t,
            // color:0xccf7ff,
            transparent:true,
            opacity:0.9
         })
    );
    quad.layers.set(1);
    quad.scale.set(vp.height * 2, vp.height, 1);
    console.log(quad);
    scene.add(quad)
})

// material
let refractionMaterial = new RefractionMaterial({
    envMap: envFbo.texture,
    resolution: [vp.width * vp.dpr, vp.height * vp.dpr]
});
let model = new THREE.Mesh(
    new THREE.BoxGeometry(1, 1, 1),
    refractionMaterial
)
scene.add(model)
///////////////////stop shading//////////////////////////////////////////
cube.rotation.x = 1
cube.rotation.y = 1

model.position.x = -1
model.position.y = -0.5
let angle = 0.003
function animate() {
    requestAnimationFrame(animate);
    renderer.clear()

    model.rotation.x += 0.005
    model.rotation.y += 0.005

    model.position.x += angle
    model.position.y += angle

    // render env to fbo
    renderer.setRenderTarget(envFbo);
    renderer.render(scene, orthoCamera);

    // render env to screen
    renderer.setRenderTarget(null);
    renderer.render(scene, orthoCamera);
    renderer.clearDepth();

    // render diamond
    renderer.render(scene, camera);
}
animate()

window.addEventListener('onmousemove', onMouseMove, false)
function onMouseMove() {
    console.log('sdf');
    renderer.setRenderTarget(envFbo);
    renderer.render(scene, orthoCamera);
}

function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize(window.innerWidth, window.innerHeight);
}


function sleep(num) {
    return new Promise(resolve => setTimeout(resolve, num))
}
