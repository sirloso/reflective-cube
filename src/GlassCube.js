const THREE = require('three')
const RefractionMaterial = require('./refraction-material')

class GlassCube {
    constructor(od, rr, resolution, cd, vp) {
        // required members for orthogonal camera
        if (!(od.left && od.right && od.top && od.bottom)) throw new Error('left,right, top or bottom member missing from od')
        // required members for webgl render target
        if (!(rr.width && rr.height)) throw new Error('Missing width or height')
        // required members for refraction material 
        if (!(resolution.width && resolution.height)) throw new Error('Resolution missing either width or height')
        // required members for cube dimensions
        if (!(cd.width && cd.height && cd.depth)) throw new Error('Cube Dimmensions missing width, height or depth')

        this.vp = vp
        this.od = od 
        this.rr = rr 
        this.cd = cd
        this.resolution = this.resolution

        this.ortho = new THREE.OrthographicCamera(od.left, od.right, od.top, od.bottom, od.near, od.far)
        this.envFbo = new THREE.WebGLRenderTarget(rr.width, rr.height)
        this.refractionMaterial = new RefractionMaterial({
            envMap: this.envFbo.texture,
            resolution: [resolution.width, resolution.height]
        })

        this.plane = new THREE.Mesh(
            new THREE.PlaneBufferGeometry(),
            new THREE.MeshBasicMaterial({
                color: new THREE.Color('red')
            })
        )
        this.plane.layers.set(1)
        this.ortho.layers.set(1)
        this.cubeGeometry = new THREE.BoxGeometry(cd.width, cd.height, cd.depth)
        this.cube = new THREE.Mesh(this.cubeGeometry,this.refractionMaterial)
    }

    static defaults() {
        let vp = {
            width: window.innerWidth,
            height: window.innerHeight,
            dpr: Math.min(devicePixelRatio, 2 || 1)
        };
        let od = {
            right: vp.width / 2,
            left: vp.width / -2,
            top: vp.height / 2,
            bottom: vp.height / -2,
        }
        let rr = {
            width: vp.width * vp.dpr,
            height: vp.height * vp.dpr,
        }
        let resolution = {
            width: vp.width * vp.dpr,
            height: vp.height * vp.dpr,
        }
        let cd = {
            width: 1,
            height: 1,
            depth: 1
        }

        return { od, rr, resolution, cd, vp }
    }
}

module.exports = {
    GlassCube
}