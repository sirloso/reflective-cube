const THREE = require('three')
const GlassCube = require('../dist/GlassCube').GlassCube


let camera,scene,renderer,background,loader

// world setup 

camera = new THREE.PerspectiveCamera(50,2)
camera.position.z = 5

renderer = new THREE.WebGLRenderer({ antialias: true })
renderer.setSize(window.innerWidth,window.innerHeight)
renderer.autoClear - false
document.body.appendChild(renderer.domElement)

scene = new THREE.Scene()
scene.background = new THREE.Color('white')

loader = new THREE.TextureLoader();

let ce = document.createElement('canvas')
let ctx = ce.getContext('2d')
ce.width = window.innerWidth
ce.height = window.innerHeight
ctx.fillRect(0,0,ce.width,ce.height)

background = new THREE.Mesh(
    new THREE.PlaneBufferGeometry(),
    new THREE.MeshBasicMaterial({
        map: new THREE.CanvasTexture(ce)
    })
)
background.layers.set(1)
background.scale.set(window.innerHeight,window.innerWidth)
scene.add(background)

//object setup
let gcd = GlassCube.defaults()
let gc  = new GlassCube(
    gcd.od,
    gcd.rr,
    gcd.resolution,
    gcd.cd,
    gcd.vp
)

// configuring glass cube 
gc.ortho.position.z = 1
gc.ortho.layers.set(1)

gc.cube.rotation.x = 1
gc.cube.rotation.y = 1
gc.cube.position.x = -1
gc.cube.position.y = -1
// scene addition
scene.add(gc.cube)

// animation 
function animation(){
    requestAnimationFrame(animation)
    renderer.clear()

    gc.cube.rotation.x += 0.005
    gc.cube.rotation.y += 0.005

    renderer.setRenderTarget(gc.envFbo)
    renderer.render(scene,gc.ortho)

    renderer.setRenderTarget(null);
    renderer.render(scene,gc.ortho)
    renderer.clearDepth();

    renderer.render(scene, camera);
}
animation()