# reflective glass cube

This project creates a threejs reflective glass cube.

### usage
```js
let GlassCubeInstance = new GlassCube( 
    orthoCameraDimensions, 
    renderTargetDimensions,
    resolutionDimensions,
    cubeDimensions,
    viewPortDimensions
)

//-or- 

let gcd = GlassCube.defaults()
let GlassCubeInstance = new GlassCube( gcd.od, gcd.rr, gcd.resolution, gcd.cd, gcd.vp )

orthoCameraDimensions = {
    right,
    left,
    top,
    bottom
}

renderTargetDimensions = {
    width,
    height
}

resolutionDimensions = {
    width,
    height
}

cubeDimensions = {
    width,
    height,
    depth
}

vp = {
    width,
    height,
    dpr
}
```

Shaders from: 
https://tympanus.net/codrops/2019/10/29/real-time-multiside-refraction-in-three-steps/